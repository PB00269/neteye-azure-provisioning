# Ansible: NetEye VM in Azure

This Ansible implementation can be used to spawn a NetEye VM on Azure.
The playbook can be run to handle the creation of the resources in Azure and to handle the VM and NetEye setup.

## Mandatory Requirements

The following requirements have been validated on a Fedora 30 machine:

-   Packages `python-pip` (`yum install python-pip`)
-   Package **Ansible v2.8.0 (or newer)** installed (`pip install ansible`)
-   The full set of dependencies required by Ansible to handle the Azure resources
-   The Azure CLI to enable the login on Azure (see https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-yum?view=azure-cli-latest for details)

The full set of python Azure package dependencies can be found in `requirements-azure.txt`.
The packages can be installed by typing `pip install -r requirements-azure.txt`


## Playbook Variables

Available variables for customizing resources in Azure can be found in `defaults/azure_defaults.yml`, while available variables for customizing NetEye can be found in `defaults/azure_defaults.yml`.


## Dependencies

None.

## Inventories

There is no need for an inventory to create the various resources in Azure.
An inventory is created from scrath by Ansible after the creation of the resources, to be used to configure NetEye.


## Playbooks

You can find two fully working playbooks, `azure_neteye.yml` and `configure_neteyevm.yml`.


To run the playbook to create the resources in Azure, type:

```sh
ansible-playbook azure_neteye.yml
```

At the end of the execution, Ansible will create an inventory file called `hosts.ini` in a new folder named with the epoch of the moment in which the playbook is triggered, inside the `inventory` folder. This inventory is used as argument for the following command:

```sh
ansible-playbook -vvvv -i inventory/<epoch>/hosts.ini configure_neteyevm.yml
```

## License


## Author Information

This paybook was created in 2020 by AVAN
