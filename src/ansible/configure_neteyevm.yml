---
- hosts: azure
  gather_facts: true
  become: yes

  any_errors_fatal: "{{ any_errors_fatal | default(true) }}"
  vars_files:
    - defaults/neteye_defaults.yml


  environment:
     ICINGAWEB_CONFIGDIR: /neteye/shared/icingaweb2/conf/


  tasks:

     # sets up hostname for the current machine by using the variables found in the inventory file
      - name: azure setup | create volume group
        lvg:
            vg: "{{ neteye_volume_group }}"
            pvs: "{{ neteye_target_disk }}"


      - name: azure setup | create logical volumes
        lvol:
            vg: "{{ neteye_volume_group }}"
            lv: "{{ item.details.0.name }}"
            size: "{{ item.details.1.size }}"
        with_items: "{{ partitions }}"


      - name: azure setup | create filesystems
        filesystem:
            dev: /dev/mapper/{{ neteye_volume_group }}-{{ item.details.0.name }}
            fstype: xfs
        with_items: "{{ partitions }}"


      - name: azure setup | check that fstab has not modified yet
        lineinfile:
            path: /etc/fstab
            line: "/dev/mapper/vg00-lvdata /data xfs defaults 0 0"
            state: present
        check_mode: yes
        register: fstab_presence

      - name: azure setup | modify fstab
        blockinfile:
            path: /etc/fstab
            backup: yes
            insertafter: EOF
            block: |
                /dev/mapper/{{ neteye_volume_group }}-{{ item.details.0.name }} /{{ item.volume }} xfs defaults 0 0
            marker: "# {mark} ANSIBLE MANAGED BLOCK {{ item.volume }}"
        with_items: "{{ partitions }}"
        when: fstab_presence is changed


      - name: azure setup | create mountpoints
        file:
            path: "/{{ item.volume }}"
            state: directory
            mode: '1777'
        when:
            item.volume == "tmp"
        with_items: "{{ partitions }}"


      - name: azure setup | create mountpoints (not tmp)
        file:
            path: "/{{ item.volume }}"
            state: directory
        when:
            item.volume != "tmp"
        with_items: "{{ partitions }}"


      - name: azure setup | mount everything
        command: mount -a
        args:
            warn: no


      - name: azure setup | install NetEye repo definition
        yum:
            name: "{{ neteye_repo_definition }}"
            state: present


      - name: azure setup | check if backup file exists before applying substitutions
        find:
            paths: /etc/yum.repos.d/
            patterns: 'CentOS-Base.repo.*'
        register: repo_definition_backup


      - name: azure setup | apply sed to repo definitions
        replace:
            path: /etc/yum.repos.d/CentOS-Base.repo
            regexp: '^(.+)$'
            replace: '#\1'
            backup: yes
        when: repo_definition_backup.matched == 0
        #command: sed -i -e 's/^/#/' /etc/yum.repos.d/CentOS-Base.repo


      # installs dependencies required
      # https://github.com/ansible/ansible/pull/31450#issuecomment-352889579
      - name: azure setup | clean yum cache
        command: "yum clean all"


      - name: azure setup | install core groups
        yum:
          name: "{{ neteye_groups_to_install }}"
          enablerepo: "{{ neteye_base_repo }}"
        #with_items: "{{ neteye_groups_to_install }}"


      # sets up hostname for the current machine by using the variables found in the inventory file
      - name: azure setup | setup custom hostname
        hostname:
            name: "{{ neteye_hostname }}.{{ neteye_domain }}"


        # checks the /etc/hosts file
      - name: azure setup | check the /etc/hosts file
        lineinfile:
            path: /etc/hosts
            line: "{{ inventory_hostname }} {{ neteye_hostname }}.{{ neteye_domain }} {{ neteye_hostname }}"
            state: present
        check_mode: yes
        register: presence


      - name: azure setup | update /etc/hosts
        lineinfile:
            path: /etc/hosts
            line: "{{ inventory_hostname }} {{ neteye_hostname }}.{{ neteye_domain }} {{ neteye_hostname }}"
        when: presence is changed

      - name: azure setup | change selinux settings
        selinux:
            policy: targeted
            state: permissive

      - name: azure setup | restart via systemd
        systemd:
          name: mariadb
          state: restarted
        async: 180
        poll: 10
        register: mariadb_shutdown
        ignore_errors: true

      # restarts mariadb
      - name: azure setup | restart mariadb before starting secure installation
        block:
          - name: neteye setup | print status of mariadb shutdown
            debug:
              msg: "{{ mariadb_shutdown }}"

          # bug: mariadb hangs while running shutdown
          - name: azure setup | get running mariadb processes list
            shell: "ps -few | grep mariadb | awk '{print $2}'"
            register: mariadb_running_instances
            ignore_errors: true

          - name: azure setup | try Kill running mariadb processes
            shell: "kill {{ item }}"
            with_items: "{{ mariadb_running_instances.stdout_lines }}"
            ignore_errors: true

          - wait_for:
              path: "/proc/{{ item }}/status"
              state: absent
              timeout: 120
            with_items: "{{ mariadb_running_instances.stdout_lines }}"
            ignore_errors: true
            register: still_running_instances

          - name: neteye setup | force kill stuck mariadb processes
            shell: "kill -9 {{ item }}"
            with_items: "{{ still_running_instances.results | select('failed') | map(attribute='item') | list }}"
            ignore_errors: true

          - name: neteye setup | restart mariadb
            systemd:
             name: mariadb
             state: restarted

        when: mariadb_shutdown.failed == true

      # starts base neteye services
      - name: azure setup | start base services
        systemd:
          name: "{{ item }}"
          state: started
        with_items: "{{ neteye_base_services }}"

      # performs first_setup script
      - name: azure setup | perform first setup script
        command: "{{ neteye_installation_script }}"
        register: secure_install
        ignore_errors: true

      # runs neteye start
      - name: azure setup | run neteye
        command: neteye start
