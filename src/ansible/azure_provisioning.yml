---
- hosts: localhost
  connection: local
  gather_facts: true

  any_errors_fatal: "{{ any_errors_fatal | default(true) }}"
  vars_files:
    - defaults/azure_defaults.yml

  tasks:
    - name: azure | register epoch timestamp
      set_fact:
        epoch_timestamp: "{{ ansible_date_time.epoch }}"


    - name: azure | create public IP address
      azure_rm_publicipaddress:
        resource_group: "{{ wp_resource_group }}"
        allocation_method: Static
        name: "{{ wp_ip_resource_name }}-{{ epoch_timestamp }}"
      register: output_ip_address


    - name: azure | register public ip name
      set_fact:
          public_ip_name: "{{ wp_ip_resource_name }}-{{ epoch_timestamp }}"


    - name: azure | dump the public ip
      debug:
          msg: "public ip {{ output_ip_address.state.ip_address }}"


    - name: azure | create security group that allows SSH/HTTP/HTTPS/Icinga2
      azure_rm_securitygroup:
        resource_group: "{{ wp_resource_group }}"
        name: "{{ wp_sg_resource_name }}-{{ epoch_timestamp }}"
        rules:
          - name: SSH
            protocol: Tcp
            destination_port_range: 22
            access: Allow
            priority: 1001
            direction: Inbound
            source_address_prefix: 82.193.25.0/24
          - name: HTTPS
            protocol: Tcp
            destination_port_range: 443
            access: Allow
            priority: 1002
            direction: Inbound
            source_address_prefix: 82.193.25.0/24
          - name: HTTP
            protocol: Tcp
            destination_port_range: 80
            access: Allow
            priority: 1003
            direction: Inbound
            source_address_prefix: 82.193.25.0/24
          - name: Icinga2
            protocol: Tcp
            destination_port_range: 5665
            access: Allow
            priority: 1004
            direction: Inbound
            source_address_prefix:
              - 82.193.25.0/24
              - "{{ output_ip_address.state.ip_address }}"
      register: output_security_group


    - name: azure | register security group name
      set_fact:
        security_group_name: "{{ wp_sg_resource_name }}-{{ epoch_timestamp }}"


    - name: azure | create virtual network interface card
      azure_rm_networkinterface:
        resource_group: "{{ wp_resource_group }}"
        name: "{{ wp_nic_resource_name }}-{{ epoch_timestamp }}"
        virtual_network: "{{ wp_virt_network }}"
        subnet: default
        ip_configurations:
          - name: default
            public_ip_address_name: "{{ public_ip_name }}"
        security_group: "{{ security_group_name }}"
      register: output_nic


    - name: azure | register nic name
      set_fact:
        nic_name: "{{ wp_nic_resource_name }}-{{ epoch_timestamp }}"


    - name: azure | generate random password for the admin
      set_fact:
          admin_password: "{{ lookup('password', '/dev/null length=25 chars=ascii_letters,digits') }}"


    - name: azure | print the admin password
      debug:
          msg: "{{ admin_password }}"


    - name: azure | create VM from scratch
      azure_rm_virtualmachine:
        resource_group: "{{ wp_resource_group }}"
        name: "{{ wp_vm_resource_name }}-{{ epoch_timestamp }}"
        vm_size: Standard_B2ms
        managed_disk_type: Standard_LRS
        admin_username: "{{ admin_user }}"
        admin_password: "{{ admin_password }}"
        ssh_password_enabled: true
        network_interfaces: "{{ nic_name }}"
        image:
          offer: CentOS
          publisher: OpenLogic
          sku: '7.7'
          version: latest
      register: output_vm


    - name: azure | register vm name
      set_fact:
        vm_name: "{{ wp_vm_resource_name }}-{{ epoch_timestamp }}"


    - name: azure | create managed disk for NetEye 4
      azure_rm_manageddisk:
        name: "{{ wp_disk_resource_name }}-{{ epoch_timestamp }}"
        resource_group: "{{ wp_resource_group }}"
        disk_size_gb: 40
        managed_by: "{{ vm_name }}"
      register: output_managed_disk


    - name: azure | register nic name
      set_fact:
        disk_name: "{{ wp_disk_resource_name }}-{{ epoch_timestamp }}"


    - name: azure | pretty printing created resources
      vars:
        msg: |
          public ip {{ public_ip_name }}
          security group {{ security_group_name }}
          nic {{ nic_name }}
          vm {{ vm_name }}
          disk {{ disk_name }}
      debug:
        msg: "{{ msg.split('\n') }}"


    - name: azure | get facts for a public ip
      azure_rm_publicipaddress_info:
          resource_group: "{{ wp_resource_group }}"
          name: "{{ public_ip_name }}"
      register: public_ip_facts


    - name: azure | print public ip facts
      debug:
          msg: "{{ public_ip_facts }}"


    # creates the folder for the current inventory
    - name: azure setup | create inventory directory if needed
      file:
        path: "inventory/{{ epoch_timestamp }}"
        state: directory

    # creates the new inventory file and adds section header
    - name: azure setup | append entry line on updated inventory file
      lineinfile:
        path: "inventory/{{ epoch_timestamp }}/hosts.ini"
        line: "[azure]"
        create: yes

    # write inventory
    - name: azure setup | write new dynamic inventory on file
      lineinfile:
          path: "inventory/{{ epoch_timestamp }}/hosts.ini"
          line: "{{ public_ip_facts.publicipaddresses[0].ip_address }} ansible_connection=ssh ansible_user={{ admin_user }} ansible_ssh_pass={{ admin_password }} ansible_sudo_pass={{ admin_password }}"
