
Name:    neteye-azure-provisioning
Version: 0.0.1
Release: 1
Summary: neteye-azure-provisioning Package

Group:	 Applications/System
License: GPL v3
Source0: %{name}.tar.gz
BuildArch: noarch

%description
%{summary}

%prep
%setup -c

%build

%install
mkdir -p %{buildroot}

%files

%changelog
* mer ago 05 2020 Andrea Avancini <andrea.avancini@wuerth-phoenix.com> - 0.0.1-1
 - Initial release
